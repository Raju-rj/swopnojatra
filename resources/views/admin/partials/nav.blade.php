<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a class="left-sidebar-toggle" href="#">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class="active"><a href="index.html"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                        </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Field Type Elements</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/type/create') }}">Add Type</a>
                                </li>
                                <li><a href="{{ url('/type') }}">Manage Type</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Question Elements</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/question/create') }}">Add Question</a>
                                </li>
                                <li><a href="{{ url('/question') }}">Manage Question</a>
                                </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Question Option</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/option/create') }}">Add Question Option</a>
                                </li>
                                <li><a href="{{ url('/option') }}">Manage Question Option</a>
                                </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Business Category </span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/business-catagory/create') }}">Add Business Category </a>
                                </li>
                                <li><a href="{{ url('/business-catagory') }}">Manage Business Category </a>
                                </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>Business Type</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/business-type/create') }}">Add Business Type</a>
                                </li>
                                <li><a href="{{ url('/business-type') }}">Manage Business Type</a>
                                </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-my-location"></i><span>Location</span></a>
                            <ul class="sub-menu">
                                <li class="parent"><a href="#"><i class="icon mdi mdi-city"></i> Division</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/division/create') }}">Add Division</a>
                                        </li>
                                        <li><a href="{{ url('/division') }}">Manage Division</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="parent"><a href="#"><i class="icon mdi mdi-city"></i> District</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/district/create') }}">Add District</a>
                                        </li>
                                        <li><a href="{{ url('/district') }}">Manage District</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="parent"><a href="#"><i class="icon mdi mdi-city"></i> Thana</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/thana/create') }}">Add Thana</a>
                                        </li>
                                        <li><a href="{{ url('/thana') }}">Manage Thana</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="parent"><a href="#"><i class="icon mdi mdi-city"></i> SubOffice</a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ url('/sub-office/create') }}">Add SubOffice</a>
                                        </li>
                                        <li><a href="{{ url('/sub-office') }}">Manage SubOffice</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-format-bold"></i><span>Bank</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/bank/create') }}">Add Bank</a>
                                </li>
                                <li><a href="{{ url('/bank') }}">Manage Bank</a>
                                </li>
                            </ul>
                        </li>

                        <li class="parent"><a href="#"><i class="icon mdi mdi-format-bold"></i><span>Branch</span></a>
                            <ul class="sub-menu">
                                <li><a href="{{ url('/branch/create') }}">Add Branch</a>
                                </li>
                                <li><a href="{{ url('/branch') }}">Manage Branch</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>