<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from foxythemes.net/preview/products/beagle/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Sep 2018 12:39:20 GMT -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('/')}}admin/img/logo-fav.png">
    <title>Beagle</title>
    <link rel="stylesheet" type="text/css" href="{{asset('/')}}admin/lib/perfect-scrollbar/css/perfect-scrollbar.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/')}}admin/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/')}}admin/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/')}}admin/lib/jqvmap/jqmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/')}}admin/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="{{asset('/')}}admin/css/app.css" type="text/css"/>
    <script src="{{asset('/')}}js/vue.js" type="text/javascript"></script>

</head>
<body>
<div class="be-wrapper be-fixed-sidebar">

    {{--header content hear--}}
    @include('admin-layout.partials.header')
    navbar content hear

            @include('admin-layout.partials.nav')
    @yield('content')
</div>
<script src="{{asset('/')}}admin/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/js/app.js" type="text/javascript"></script>


<script src="{{asset('/')}}admin/lib/jquery-flot/jquery.flot.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-flot/jquery.flot.pie.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-flot/jquery.flot.time.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-flot/jquery.flot.resize.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-flot/plugins/jquery.flot.orderBars.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-flot/plugins/curvedLines.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-flot/plugins/jquery.flot.tooltip.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/countup/countUp.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jqvmap/jquery.vmap.min.js" type="text/javascript"></script>
<script src="{{asset('/')}}admin/lib/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>




<script type="text/javascript">
    $(document).ready(function(){
        //-initialize the javascript
        App.init();
        App.dashboard();

    });
</script>
</body>

<!-- Mirrored from foxythemes.net/preview/products/beagle/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Sep 2018 12:39:29 GMT -->
</html>