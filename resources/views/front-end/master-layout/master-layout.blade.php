<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from wp3.commonsupport.com/html/varna-charity/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 08 Oct 2018 15:29:40 GMT -->
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>

    <!-- Stylesheets -->
    <link href="front-end/css/bootstrap.css" rel="stylesheet">
    <link href="front-end/plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
    <link href="front-end/plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
    <link href="front-end/plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css">
    <link href="front-end/css/style.css" rel="stylesheet">
    <link href="front-end/css/responsive.css" rel="stylesheet">

    <link rel="shortcut icon" href="front-end/images/favicon.png" type="image/x-icon">
    <link rel="icon" href="front-end/images/favicon.png" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="front-end/js/respond.js"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    {{--header option--}}
    @include('front-end.partials.header')

    {{--main section--}}
    @yield('content')

    {{--footer section--}}
    @include('front-end.partials.footer')

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="front-end/js/jquery.js"></script>
<script src="front-end/js/bootstrap.min.js"></script>

<!--Revolution Slider-->
<script src="front-end/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="front-end/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="front-end/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="front-end/js/main-slider-script.js"></script>
<!--End Revolution Slider-->


<script src="front-end/js/jquery-ui.js"></script>
<script src="front-end/js/jquery.fancybox.js"></script>
<script src="front-end/js/owl.js"></script>
<script src="front-end/js/wow.js"></script>
<script src="front-end/js/appear.js"></script>
<script src="front-end/js/script.js"></script>
</body>

<!-- Mirrored from wp3.commonsupport.com/html/varna-charity/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 08 Oct 2018 15:29:41 GMT -->
</html>
