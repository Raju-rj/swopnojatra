<footer class="main-footer" style="background-image: url(images/background/2.jpg);">
    <!--Upper-->
    <div class="footer-upper">
        <div class="auto-container">
            <div class="outer-box">
                <!--Footer Logo-->
                <div class="footer-logo"><a href="index-2.html"><img src="images/footer-logo.png" alt=""></a></div>

                <div class="row clearfix">
                    <div class="big-column col-md-5 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <h3>Menu</h3>
                                <ul class="footer-links">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Program</a></li>
                                    <li><a href="#">Portfolio</a></li>
                                    <li><a href="#">Updates</a></li>
                                    <li><a href="#">Donate</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                </ul>
                            </div>

                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <h3>Follow</h3>
                                <ul class="footer-links">
                                    <li><a href="#">Facebook</a></li>
                                    <li><a href="#">Twitter</a></li>
                                    <li><a href="#">Google+</a></li>
                                    <li><a href="#">Instagram</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="big-column col-md-7 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <h3>Contact </h3>
                                <ul class="footer-links contact-links">
                                    <li>121 King Street, Melbourne, <br> Australia</li>
                                    <li><a href="#">+123 456 789 000</a></li>
                                    <li><a href="#">info@varna.com+</a></li>
                                </ul>
                            </div>

                            <!--Footer Column-->
                            <div class="footer-column col-md-6 col-sm-6 col-xs-12">
                                <h3>About</h3>
                                <div class="text">
                                    <p>Capitalize on low hanging fruit to identify value added activityto beta test. Override the digital divide with additional clickthroughs from</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Bottom-->
    <div class="footer-bottom">
        <div class="auto-container">
            <div class="copyright">Copyright <a href="index-2.html">Varna Charity</a> © 2018. All Rights Reserved.</div>
        </div>
    </div>
</footer>