<header class="main-header header-style-two">
    <!--Header Top-->
    <div class="header-top">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="top-left pull-left">
                    <ul class="contact-info">
                        <li><a href="#"><span class="fa fa-envelope"></span> hello@varna.com</a></li>
                        <li><a href="#"><span class="fa fa-phone"></span> + 1800 0289 4894</a></li>
                    </ul>
                </div>
                <div class="top-right pull-right clearfix">
                    <div class="lang"><a href="#"><span class="fa fa-globe"></span> Eng</a></div>
                    <div class="login-info">
                        <a href="#"><span class="fa fa-sign-in"></span>Donor Login</a>
                        <a href="#">Non-Profit Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Header Top -->

    <!-- Header Lower -->
    <div class="header-lower">
        <div class="auto-container">
            <div class="main-box clearfix">
                <!--Logo Box-->
                <div class="logo-box">
                    <div class="logo"><a href="index-2.html"><img src="front-end/images/logo-2.png" alt=""></a></div>
                </div>

                <!--Nav Outer-->
                <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class=""><a href="index-2.html">Home</a>

                                </li>
                                <li class="current"><a href="about.html">About</a></li>
                                <li><a href="services.html">Program</a></li>
                                <li class="dropdown"><a href="#">Portfolio</a>
                                    <ul>
                                        <li><a href="causes-grid.html">Causes Grid View</a></li>
                                        <li><a href="causes-list.html">Causes List View</a></li>
                                        <li><a href="causes-single.html">Cause Details</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="#">Updates</a>
                                    <ul>
                                        <li><a href="event-list.html">Event List View</a></li>
                                        <li><a href="event-grid.html">Event Grid View</a></li>
                                        <li><a href="event-single.html">Event Single Post</a></li>
                                    </ul>
                                </li>
                                <li class=""><a href="#">DONATE</a></li>
                                <li><a href="contact.html">Contact Us</a></li>
                            </ul>
                        </div>
                    </nav>
                    <!-- Main Menu End-->

                    <div class="option-box">
                        <div class="language dropdownn"><a class="btn btn-default dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="#"><span class="flag-icon"><img src="images/icons/flag-icon.jpg" alt="" /></span> En</a>
                            <ul class="dropdown-menu style-one" aria-labelledby="dropdownMenu1">
                                <li><a href="#">Arabic</a></li>
                                <li><a href="#">China</a></li>
                                <li><a href="#">German</a></li>
                                <li><a href="#">French</a></li>
                            </ul>
                        </div>

                        <!--Search Box-->
                        <div class="search-box-outer">
                            <!--Search Box-->
                            <div class="dropdown dropdown-outer">
                                <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flaticon-magnifying-glass"></span></button>
                                <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                    <li class="panel-outer">
                                        <div class="form-container">
                                            <form method="post" action="http://wp3.commonsupport.com/html/varna-charity/blog.html">
                                                <div class="form-group">
                                                    <input type="search" name="field-name" value="" placeholder="Search Here" required="">
                                                    <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div><!--End earch Box-->
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>