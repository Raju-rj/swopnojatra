@extends('front-end.master-layout.master-layout')
@section('title','SWOPNOJATRA | FOUNDATION')
@section('content')

    <!--Main Slider-->
    <section class="main-slider">
        <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
            <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
                <ul>
                    <!-- Slide 1 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1691" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="front-end/images/main-slider/image-1.jpg" data-title="Slide Title" data-transition="parallaxvertical">

                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="front-end/images/main-slider/image-4.jpg">

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="none"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-135','-135','-135','-135']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h4>Donate Here</h4>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-fontsize="['64','40','36','24']"
                             data-width="auto"
                             data-textalign="center"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-30','-30','-30','-30']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2>Help poor people life <br>and their <span class="style-font">formation</span></h2>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="auto"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['115','105','130','130']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="about.html" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>
                    <!-- Slide 2 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="front-end/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">

                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="front-end/images/main-slider/image-1.jpg">

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="none"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-135','-135','-135','-135']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h4>Donate Here</h4>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-fontsize="['64','40','36','24']"
                             data-width="auto"
                             data-textalign="center"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-30','-30','-30','-30']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2>Help poor people life <br>and their <span class="style-font">formation</span></h2>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="auto"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['115','105','130','130']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="about.html" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=mmkm0NS6a14" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>

                    <!-- Slide 3 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1689" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="front-end/images/main-slider/image-3.jpg" data-title="Slide Title" data-transition="parallaxvertical">

                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="front-end/images/main-slider/image-2.jpg">

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="none"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-135','-135','-135','-135']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h4>Donate Here</h4>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-fontsize="['64','40','36','24']"
                             data-width="auto"
                             data-textalign="center"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-30','-30','-30','-30']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2>Help poor people life <br>and their <span class="style-font">formation</span></h2>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="auto"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['115','105','130','130']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="about.html" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>

                    <!-- Slide 4 -->
                    <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1690" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="front-end/images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">

                        <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="front-end/images/main-slider/image-3.jpg">

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="none"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-135','-135','-135','-135']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h4>Donate Here</h4>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-fontsize="['64','40','36','24']"
                             data-width="auto"
                             data-textalign="center"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['-30','-30','-30','-30']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <h2>Help poor people life <br>and their <span class="style-font">formation</span></h2>
                        </div>

                        <div class="tp-caption"
                             data-paddingbottom="[0,0,0,0]"
                             data-paddingleft="[0,0,0,0]"
                             data-paddingright="[0,0,0,0]"
                             data-paddingtop="[0,0,0,0]"
                             data-responsive_offset="on"
                             data-type="text"
                             data-height="none"
                             data-whitespace="nowrap"
                             data-width="auto"
                             data-hoffset="['0','0','0','0']"
                             data-voffset="['115','105','130','130']"
                             data-x="['center','center','center','center']"
                             data-y="['middle','middle','middle','middle']"
                             data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'>
                            <a href="about.html" class="theme-btn btn-style-one"><span>Donate Now</span></a>
                            <a href="https://www.youtube.com/watch?v=Fvae8nxzVz4" class="theme-btn play-btn" data-fancybox="gallery" data-caption=""><span class="icon fa fa-play"></span> View Video <span> 4 : 45</span></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Main Slider-->




    <!-- Work Section -->
    <section class="work-section">
        <div class="auto-container">
            <!-- Work Steps -->
            <div class="steps-outer">
                <div class="row clearfix">
                    <!-- Step -->
                    <div class="work-step col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <h2>01.</h2>
                            <h4><a href="about.html">ICT Skill Development</a></h4>
                            <p>Objectively innovate empowered tured products whereas parallel platforms. the Holisticly predominate </p>
                        </div>
                    </div>

                    <!-- Step -->
                    <div class="work-step col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <h2>02.</h2>
                            <h4><a href="about.html">Free Blood Grouping Campign</a></h4>
                            <p>Objectively innovate empowered tured products whereas parallel platforms. the Holisticly predominate </p>
                        </div>
                    </div>

                    <!-- Step -->
                    <div class="work-step col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <h2>03.</h2>
                            <h4><a href="about.html">Free Health Campaign</a></h4>
                            <p>Objectively innovate empowered tured products whereas parallel platforms. the Holisticly predominate </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Work Section -->

    <!-- About Section-->
    <section class="about-section">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Column-->
                <div class="content-column pull-right col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <span class="title">About Us</span>
                            <h2>Welcome to<span> our</span> <br>varna charity <span>please rise your</span>  helping hand</h2>
                            <div class="separator"></div>
                        </div>
                        <div class="text">Integer et diam libero. Praesent quis varius nisi. Nunc vitae est sodales, tincidunt augue ac, blandit ante. Aenean a ipsum pellentesque, ultrices nibh et, maximus nisl. Ut ante arcu, congue viverra efficitur eu, posuere quis lectus. Donec rhoncus tempus tellus nec lobortis. Vivamus iaculis mollis lectus ac imperdiet.</div>
                        <div class="our-help">
                            <ul>
                                <li><a href="causes-grid.html">ICT Skill Development</a></li>
                                <li><a href="causes-grid.html">Blood Grouping Campaign</a></li>
                                <li><a href="causes-grid.html">Worm cloth Distribution</a></li>
                                <li><a href="causes-grid.html">Umbrella Fastival</a></li>
                                <li><a href="causes-grid.html">Tree Plantation</a></li>
                            </ul>
                        </div>
                        <div class="btn-box">
                            <a href="about.html" class="theme-btn btn-style-one"><span>View More Info</span></a>
                        </div>
                    </div>
                </div>

                <!--Image Column-->
                <div class="image-column col-md-6 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <figure class="image-1"><img src="front-end/images/resource/about-img-1.jpg" alt=""></figure>
                        <figure class="image-2"><img src="front-end/images/resource/about-img-2.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End About Section-->

    <!-- Causes Section -->
    <section class="causes-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">Protfolios</span>
                <h2>Raise your <span>funds</span> for a <span>cause</span> that <br> you care</h2>
                <div class="separator-two"></div>
            </div>

            <div class="row clearfix">
                <!-- Cause Block -->
                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/ict-image01.jpg" alt=""></figure>
                            <div class="overlay-box"><a href="ict_skill.html" class="link">See More &gt;</a></div>
                        </div>
                        <div class="lower-content">
                            <h2><a href="causes-single.html">ICT Skill Development</a></h2>
                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>

                            <div class="progress-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="65"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="65">0</span>%</div></div></div></div>
                            </div>

                            <div class="info-box clearfix">
                                <a href="#" class="raised">Raised: <span>$1000</span></a>
                                <a href="#" class="goal">Goal: <span>$11000</span></a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cause Block -->
                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/cause-2.jpg" alt=""></figure>
                            <div class="overlay-box"><a href="#" class="link">Donate Now &gt;</a></div>
                        </div>
                        <div class="lower-content">
                            <h2><a href="causes-single.html">Rise For Life</a></h2>
                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>

                            <div class="progress-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="20"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="20">0</span>%</div></div></div></div>
                            </div>

                            <div class="info-box clearfix">
                                <a href="#" class="raised">Raised: <span>$1000</span></a>
                                <a href="#" class="goal">Goal: <span>$11000</span></a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cause Block -->
                <div class="cause-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/cause-3.jpg" alt=""></figure>
                            <div class="overlay-box"><a href="#" class="link">Donate Now &gt;</a></div>
                        </div>
                        <div class="lower-content">
                            <h2><a href="causes-single.html">Heart To Heart</a></h2>
                            <p>Integer et diam libero. Praesent  varius nisi. Nunc vitae est sodales.</p>

                            <div class="progress-bar">
                                <div class="bar-inner"><div class="bar progress-line" data-width="80"><div class="bar-percentage"><div class="count-box"><span class="count-text" data-speed="2000" data-stop="80">0</span>%</div></div></div></div>
                            </div>

                            <div class="info-box clearfix">
                                <a href="#" class="raised">Raised: <span>$1000</span></a>
                                <a href="#" class="goal">Goal: <span>$11000</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Causes Section -->

    <!--Fun Facts Section-->
    <div class="fun-fact-section" style="background-image:url(front-end/images/background/1.jpg);">
        <div class="auto-container">
            <div class="sec-title light text-center">
                <span class="title">Our Mission</span>
                <h2>Milestone <span>achieved</span></h2>
            </div>
            <div class="fact-counter">
                <div class="row clearfix">
                    <!--Column-->
                    <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                            <div class="count-box">
                                <span class="count-text" data-speed="3000" data-stop="12">0</span>
                                <h4 class="counter-title">Schools</h4>
                                <span class="icon flaticon-people"></span>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                            <div class="count-box">
                                <span class="count-text" data-speed="3000" data-stop="96">0</span>
                                <h4 class="counter-title">Doctors</h4>
                                <span class="icon flaticon-pacifism"></span>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                            <div class="count-box">
                                <span class="count-text" data-speed="3000" data-stop="50">0</span>
                                <h4 class="counter-title">Books</h4>
                                <span class="icon flaticon-heart-2"></span>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="counter-column col-md-3 col-sm-6 col-xs-12">
                        <div class="inner">
                            <div class="count-box">
                                <span class="count-text" data-speed="3000" data-stop="35">0</span>
                                <h4 class="counter-title">Teachars</h4>
                                <span class="icon flaticon-love-planet"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Fun Facts Section-->

    <!-- Who We Are -->
    <section class="who-we-are">
        <div class="outer-container clearfix">
            <!--Content Column-->
            <div class="content-column">
                <div class="inner-box">
                    <div class="sec-title">
                        <span class="title">Who we are</span>
                        <h2>We <span>help</span> thousands of children to get<br> their <span>education</span></h2>
                        <span class="separator-two"></span>
                    </div>
                    <div class="text">
                        <p>Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your.</p>
                    </div>
                    <ul class="choose-info">
                        <li>
                            <span class="icon flaticon-coin"></span>
                            <h3><a href="#">Make Donation</a></h3>
                            <p>Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence</p>
                        </li>

                        <li>
                            <span class="icon flaticon-shirt"></span>
                            <h3><a href="#">Become a Volunteer</a></h3>
                            <p>Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence</p>
                        </li>

                        <li>
                            <span class="icon flaticon-globe"></span>
                            <h3><a href="#">Give Scholarship </a></h3>
                            <p>Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!--Image Column-->
            <div class="image-column" style="background-image:url(front-end/images/resource/image-1.jpg);">
                <figure class="image-box"><img src="front-end/images/resource/image-1.jpg" alt=""></figure>
            </div>
        </div>
    </section>
    <!-- End Who We Are -->

    <!-- Volunteers Section -->
    <section class="volunteers-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">OUR VOLUNTEER</span>
                <h2>Our list of <span>volunteers</span> are ready <br> to <span>support</span> the difficulty in the social.</h2>
                <span class="separator-two"></span>
            </div>

            <div class="row clearfix">
                <!-- Volunteer Block -->
                <div class="volunteer-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/volunteer-1.jpg" alt=""></figure>
                        </div>
                        <div class="overlay-box">
                            <div class="content">
                                <h3 class="name"><a href="#">JULIA ANNA</a></h3>
                                <span class="designation">Engineer</span>
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Volunteer Block -->
                <div class="volunteer-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/volunteer-2.jpg" alt=""></figure>
                        </div>
                        <div class="overlay-box">
                            <div class="content">
                                <h3 class="name"><a href="#">JULIA ANNA</a></h3>
                                <span class="designation">Engineer</span>
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Volunteer Block -->
                <div class="volunteer-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/volunteer-3.jpg" alt=""></figure>
                        </div>
                        <div class="overlay-box">
                            <div class="content">
                                <h3 class="name"><a href="#">JULIA ANNA</a></h3>
                                <span class="designation">Engineer</span>
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Volunteer Block -->
                <div class="volunteer-block col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box">
                            <figure><img src="front-end/images/resource/volunteer-4.jpg" alt=""></figure>
                        </div>
                        <div class="overlay-box">
                            <div class="content">
                                <h3 class="name"><a href="#">JULIA ANNA</a></h3>
                                <span class="designation">Engineer</span>
                                <ul class="social-icon">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Volunteers Section -->

    <!-- Donation And Events -->
    <section class="donation-and-events" style="background-image: url(front-end/front-end/images/background/7.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!-- Donation Column -->
                <div class="donation-column col-md-7 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title light">
                            <span class="title">Donation</span>
                            <h2>Make a <span>donation</span> now!</h2>
                        </div>
                        <!-- Donation Form -->
                        <div class="donation-form">
                            <form>
                                <div class="row clearfix">
                                    <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                        <h4>Payment Type </h4>
                                        <input type="radio" name="one-time" id="one-time">
                                        <label for="one-time">One Time</label>
                                        <input type="radio" name="one-time" id="recurring">
                                        <label for="recurring">Recurring</label>
                                    </div>

                                    <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                        <h4>I Want to Donate for</h4>
                                        <select class="custom-select-box">
                                            <option>Educated Childrens</option>
                                            <option>Educated Childrens</option>
                                            <option>Educated Childrens</option>
                                            <option>Educated Childrens</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                        <h4>Currency</h4>
                                        <select class="custom-select-box">
                                            <option>USD , Us dollers</option>
                                            <option>USD , Us dollers</option>
                                            <option>USD , Us dollers</option>
                                            <option>USD , Us dollers</option>
                                            <option>USD , Us dollers</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                        <h4>How much do you want to donate?</h4>
                                        <select class="custom-select-box">
                                            <option>30</option>
                                            <option>50</option>
                                            <option>100</option>
                                            <option>200</option>
                                            <option>500</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 btn-box">
                                        <button type="submit" class="theme-btn btn-style-one">Donate Now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- Event Column -->
                <div class="event-column col-md-5 col-sm-12 col-xs-12">
                    <div class="inner-column">
                        <div class="sec-title light">
                            <span class="title">Event</span>
                            <h2>Upcoming <span>events</span></h2>
                        </div>
                        <!-- Event Block -->
                        <div class="event-block">
                            <div class="inner-box">
                                <div class="image-box">
                                    <img src="front-end/images/resource/event-1.jpg" alt="">
                                    <div class="date"><span>04</span> April</div>
                                </div>
                                <div class="content-box">
                                    <h4><a href="event-grid.html">Gear up for giving</a></h4>
                                    <ul class="info">
                                        <li><a href="event-grid.html"><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                        <li><a href="event-grid.html"><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination.</p>
                                </div>
                            </div>
                        </div>
                        <!-- Event Block -->
                        <div class="event-block">
                            <div class="inner-box">
                                <div class="image-box">
                                    <img src="front-end/images/resource/event-2.jpg" alt="">
                                    <div class="date"><span>07</span> April</div>
                                </div>
                                <div class="content-box">
                                    <h4><a href="event-grid.html">Sponcer a child today</a></h4>
                                    <ul class="info">
                                        <li><a href="event-grid.html"><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                        <li><a href="event-grid.html"><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination. </p>
                                </div>
                            </div>
                        </div>
                        <!-- Event Block -->
                        <div class="event-block">
                            <div class="inner-box">
                                <div class="image-box">
                                    <img src="front-end/front-end/images/resource/event-3.jpg" alt="">
                                    <div class="date"><span>12</span> April</div>
                                </div>
                                <div class="content-box">
                                    <h4><a href="event-grid.html">Food for poor</a></h4>
                                    <ul class="info">
                                        <li><a href="event-grid.html"><i class="fa fa-clock-o"></i>at 5.00 pm - 7.30 pm</a></li>
                                        <li><a href="event-grid.html"><i class="fa fa-map-marker"></i>25 Newyork City</a></li>
                                    </ul>
                                    <p>Bring to the table win-win survival strategies to ensure proactive domination.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- End Donation And Events -->

    <!-- Testimonial Section -->
    <section class="testimonial-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">Tesimonila</span>
                <h2>Our <span>client’s</span> reviews</h2>
                <div class="separator-two"></div>
            </div>
        </div>
        <div class="testimonial-carousel owl-carousel owl-theme">
            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-1.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-2.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-3.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-1.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-2.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-3.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-1.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-2.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>

            <!-- Testimonial Block -->
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="thumb"><img src="front-end/images/resource/thumb-3.jpg" alt=""></div>
                    <p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
                    <h5 class="name"><a href="#">Nattasha</a></h5><span class="designation">, Manager</span>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Section -->

    <!-- News Section -->
    <section class="news-section">
        <div class="auto-container">
            <div class="sec-title text-center">
                <span class="title">Blog</span>
                <h2>Our latest <span>news</span></h2>
                <div class="separator-two"></div>
            </div>

            <div class="row clearfix">
                <!-- News Blcok -->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box"><a href="blog-classic.html"><img src="front-end/images/resource/news-1.jpg" alt=""></a></div>
                        <div class="lower-content">
                            <ul class="info-box">
                                <li><span class="icon flaticon-user-1"></span>By<a href="#">Admin</a></li>
                                <li><span class="icon flaticon-like-1"></span>35<a href="#">Likes</a></li>
                                <li><span class="icon flaticon-speech-bubble-and-three-dots"></span>23<a href="#">Comment</a></li>
                            </ul>
                            <h3><a href="blog-single-1.html">This Is A Standard Post With Thumbnail</a></h3>
                            <p>Capitalize on low hanging fruit to identify value added activity to beta test. Override the digital divide with additional clickthroughs from</p>
                            <a href="blog-single-1.html" class="read-more">Read More</a>
                        </div>
                    </div>
                </div>

                <!-- News Blcok -->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box"><a href="blog-classic.html"><img src="front-end/images/resource/news-2.jpg" alt=""></a></div>
                        <div class="lower-content">
                            <ul class="info-box">
                                <li><span class="icon flaticon-user-1"></span>By<a href="#">Admin</a></li>
                                <li><span class="icon flaticon-like-1"></span>35<a href="#">Likes</a></li>
                                <li><span class="icon flaticon-speech-bubble-and-three-dots"></span>23<a href="#">Comment</a></li>
                            </ul>
                            <h3><a href="blog-single-1.html">This Is A Standard Post With Thumbnail</a></h3>
                            <p>Capitalize on low hanging fruit to identify value added activity to beta test. Override the digital divide with additional clickthroughs from</p>
                            <a href="blog-single-1.html" class="read-more">Read More</a>
                        </div>
                    </div>
                </div>

                <!-- News Blcok -->
                <div class="news-block col-md-4 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <div class="image-box"><a href="blog-classic.html"><img src="front-end/images/resource/news-3.jpg" alt=""></a></div>
                        <div class="lower-content">
                            <ul class="info-box">
                                <li><span class="icon flaticon-user-1"></span>By<a href="#">Admin</a></li>
                                <li><span class="icon flaticon-like-1"></span>35<a href="#">Likes</a></li>
                                <li><span class="icon flaticon-speech-bubble-and-three-dots"></span>23<a href="#">Comment</a></li>
                            </ul>
                            <h3><a href="blog-single-1.html">This Is A Standard Post With Thumbnail</a></h3>
                            <p>Capitalize on low hanging fruit to identify value added activity to beta test. Override the digital divide with additional clickthroughs from</p>
                            <a href="blog-single-1.html" class="read-more">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End News -->

    <!-- Subscribe section -->
    <section class="subscribe-section">
        <div class="auto-container">
            <div class="inner-container">
                <div class="row clearfix">
                    <div class="title-column col-md-6 col-sm-12 col-xs-12">
                        <div class="title">
                            <h2> Hotline :  +880 188779191</h2>
                        </div>
                    </div>
                    <div class="form-column col-md-6 col-sm-12 col-xs-12">
                        <div class="subscribe-form">
                            <form method="post" action="http://wp3.commonsupport.com/html/varna-charity/contact.html">
                                <div class="form-group">
                                    <input type="email" name="email" value="" placeholder="Enter Your email" required>
                                    <button type="submit" class="theme-btn btn-style-three">Send Mail</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Subscribe section -->

@endsection